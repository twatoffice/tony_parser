<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Parse_bifoo extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        session_start();
        $this->load->helper('date');
        date_default_timezone_set('Asia/Taipei');
        ini_set('memory_limit', '256M');
        set_time_limit(0);
        $this->load->Model("Show_db_model");//載入model

    }

    public function parse($type, $target) {

        //load parse套件
        $this->load->library('simple_html_dom');

        $html = file_get_html($target);

        if ($type == '抓熱門股100筆') {
            $result = $html->find('table[width=1258] a');
        } elseif ($type == '個別抓取各公司股價') {
            $result = $html->find('table[width=100%]',3);
        } elseif ($type == '新增一間公司資料至資料庫') {
            $result = $html->find('table[width=100%]', 3);
        } elseif ($type == '從趨勢圖抓歷史股價') {
            $result = $html->find('script', 0);
        }
        
        return $result;
    }

    public function auto_update() {

        //會將錯誤、警告訊息顯示在網頁上 ex: https://clayclay76.tian.yam.com/posts/29151418
        ini_set('display_errors', 'On');

        //開始計算時間
        $time_start = microtime(true);



        //排行榜>掛單排行榜 TOP1-100
        $type = '抓熱門股100筆';
        $target = 'http://www.berich.com.tw/DP/OrderList/List_kakelu.asp';
        
        $result = $this->parse($type, $target);

        

        $array = array();

        foreach($result as $v) {
            
            $url_data = substr($v->href, 2);
            //echo $url_data."<br>";
            $bf_cmpname = substr($url_data, 0, 35);
            // echo $bf_cmpname."<br>";
            $cmpname = substr($url_data, 35);
            // echo $cmpname."<br>";
            //new_str為utf8轉為big5後的亂碼
            $new_str = iconv("UTF-8", "BIG-5", $cmpname);
            /*
            將轉為big5後的亂碼用urlencode後,就是網址看的懂的東西了
            找超久!!!
            */
            $big = urlencode($new_str);
            //echo $cmpname.", ",$new_str.", ".$big."<br>";
            $cmp_url = 'http://www.berich.com.tw/DP'.$bf_cmpname.$big;
            $cmp_array = array(
                            'cmp_url' => $cmp_url,
                            'cmpname' => $cmpname);

            //把熱門股所有網址放入array
            array_push($array, $cmp_array);
            
        }


        //從熱門股100筆各自之網址抓取內文資料
        for ($i = 0; $i < count($array); $i++) { 
            //echo $array[$i]['cmpname'];
            $cmpname = substr($array[$i]['cmp_url'], 35);
            //echo "<br>".$array[$i]['cmp_url']."<br>";

            //把熱門股網址array一一帶入至$html
            $type = '個別抓取各公司股價';
            $target = $array[$i]['cmp_url'];
            $table = $this->parse($type, $target);
            //echo $html->plaintext;
            foreach($table->find('tr') as $row) {
                // initialize array to store the cell data from each row
                $flight = array();
                foreach($row->find('td') as $cell) {
                    // push the cell's text to the array
                    $flight[] = $cell->plaintext;
                }
                $rowData[] = $flight;
            }
        }


        //把撈到的資料放入陣列data
        $data = array();
        for ( $i=0; $i<count($rowData); $i++) { 
            if ($i%2 == 1) {
                array_push($data, $rowData[$i]);
            }
        }


        //把公司名稱加進陣列data
        for ($i=0; $i < count($data); $i++) { 
            switch ($array[$i]['cmpname']) {
                case '永達保險經紀':
                    $array[$i]['cmpname'] = '永達保險';
                    break;
                case '陽信商業銀行':
                    $array[$i]['cmpname'] = '陽信商銀';
                    break;
                case '大銀微系統':
                    $array[$i]['cmpname'] = '大銀微系';
                    break;
                case '昱鐳光電科技':
                    $array[$i]['cmpname'] = '昱鐳光電';
                    break;
                case '國璽幹細胞應用技術':
                    $array[$i]['cmpname'] = '國璽幹細';
                    break;
                case '南美特科技':
                    $array[$i]['cmpname'] = '南美特科';
                    break;
                case '太平洋崇光百貨':
                    $array[$i]['cmpname'] = '崇光百貨';
                    break;
                case '優陽材料科技':
                    $array[$i]['cmpname'] = '優陽材料';
                    break;
                case '賽亞基因科技':
                    $array[$i]['cmpname'] = '賽亞基因';
                    break;
                case '華泰商業銀行':
                    $array[$i]['cmpname'] = '華泰銀行';
                    break;
                case '台中精機廠':
                    $array[$i]['cmpname'] = '台中精機';
                    break;
                case '華聯生物科技':
                    $array[$i]['cmpname'] = '華聯生技';
                    break;
                case '板信商業銀行':
                    $array[$i]['cmpname'] = '板信商銀';
                    break;
                case '台灣之星電信':
                    $array[$i]['cmpname'] = '台灣之星';
                    break;
                case '必翔電能高科技':
                    $array[$i]['cmpname'] = '必翔電能';
                    break;
                case '中華海洋生技':
                    $array[$i]['cmpname'] = '中華海洋';
                    break;
                case '宣捷幹細胞生技':
                    $array[$i]['cmpname'] = '宣捷幹細胞';
                    break;
                case '善德生化':
                    $array[$i]['cmpname'] = '善德生技';
                    break;
                case '台灣龍盟科技':
                    $array[$i]['cmpname'] = '台灣龍盟';
                    break;
                case '台灣集中保管結算所':
                    $array[$i]['cmpname'] = '台灣集保';
                    break;
                case '中國生化科技':
                    $array[$i]['cmpname'] = '中國生化';
                    break;
                case '旭暉應用材料':
                    $array[$i]['cmpname'] = '旭暉應用';
                    break;
                case '台灣奈米碳素':
                    $array[$i]['cmpname'] = '台灣奈米';
                    break;
                case '台亞衛星通訊':
                    $array[$i]['cmpname'] = '台亞衛星';
                    break;
                case '華矽半導體':
                    $array[$i]['cmpname'] = '華矽半導';
                    break;
                case '中華立鼎光電':
                    $array[$i]['cmpname'] = '中華立鼎';
                    break;
                case '格上汽車租賃':
                    $array[$i]['cmpname'] = '格上租車';
                    break;
                case '宣捷生物科技':
                    $array[$i]['cmpname'] = '宣捷生技';
                    break;
                case '大無畏全球投資控股':
                    $array[$i]['cmpname'] = '大無畏控股';
                    break;
                case '京華堂實業':
                    $array[$i]['cmpname'] = '京華堂';
                    break;
                case '台塑網科技':
                    $array[$i]['cmpname'] = '台塑網科';
                    break;
                case '大中票券金融':
                    $array[$i]['cmpname'] = '大中票券';
                    break;
                case '新光三越百貨':
                    $array[$i]['cmpname'] = '新光三越';
                    break;
                case '太平洋電線電纜':
                    $array[$i]['cmpname'] = '太電電纜';
                    break;

                default:
                    # code...
                    break;
            }
            array_push($data[$i], $array[$i]['cmpname']);
            array_push($data[$i], $array[$i]['cmp_url']);
            if ($data[$i][4] == '議價' || $data[$i][4] == '-') {
                $data[$i][4] = -2;
            }
            if ($data[$i][11] == '議價' || $data[$i][11] == '-') {
                $data[$i][11] = -2;
            }
        }

        //連接資料庫
        $conn = $this->local_server_environment();


        $data = $this->if_in_database($array, $data, $conn);
        //False則尚無此公司資料


        //將手動設定暫停更新的公司剔除array外
        $this->load->Model("Parse_model");//載入model
        $setting_list = $this->Parse_model->get_parse_setting();


        for ($i=0; $i < count($setting_list); $i++) {  
            for ($j=0; $j < count($array); $j++) { 

                if ($array[$j]['cmpname'] == $setting_list[$i]['公司簡稱']) {
                    
                    $sql = "SELECT Top 1 * FROM Shares WHERE CompanyID = ".$data[$j][16]." ORDER BY [Date] DESC";
                    
                    $company_list = sqlsrv_query($conn, $sql);
                    $temp = sqlsrv_fetch_array($company_list);

                    $data_date = substr($temp[5], 0, 10);
                    $today = date("Y-m-d");
                    if ($data_date == $today) {
                    //最新日期為當日 用update
                        $sql = "UPDATE Shares SET [Date] = CONVERT(varchar(100), GETDATE(), 23) WHERE SharesID = ".$temp[0];
                        echo "<br>".$sql;
                        sqlsrv_query($conn, $sql); 
                    } else {
                    //最新日期非當日 用insert
                        $sql = "INSERT into Shares (CompanyID, AP, SP, PS, [Date]) values (".$data[$j][16].", ".$temp[2].", ".$temp[3].", NULL, CONVERT(varchar(100), GETDATE(), 23) )";
                        echo "<br>".$sql;
                        sqlsrv_query($conn, $sql); 
                    }
                    echo "<br>";






                    echo "<h4><font color='red'>".$setting_list[$i]['公司簡稱']."</font> 目前已暫停更新</h4>";
                    unset($array[$j]);
                    unset($data[$j]);
                    $array = array_values($array);
                    $data = array_values($data);
                }
            }
        }
        //結束


        $datetime= date("Y/m/d H:i:s");
        echo "更新時間 : ".$datetime."<br><br>";

        for ($i=0; $i < count($data); $i++) { 
            echo "<br>".$data[$i][16];
            if (is_null($data[$i][16])) {
                echo "<br>沒有資料: ".$array[$i]['cmpname'];
                // $this->insert_info($data[$i], $conn);
            } else {
                echo "<br>".$array[$i]['cmpname'];
                $this->update_info($array, $data[$i], $conn);
            }
        }
        echo "<br>";

        // Close the connection.
        sqlsrv_close($conn);

        //計算執行時間
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        echo '<br><br>此次更新共花費 '.$time."秒<br>";


        $this->load->view('parse_view');
    }

    //local端database
    public function local_server_environment() {
        $serverName = "192.168.147.205,8001";
        // $serverName = "localhost";
        $uid = 'kcc888Web';
        $pwd = 'dafaco#1!a0989646614';
        $connectionInfo = array( "UID"=>$uid, "PWD"=>$pwd, "Database"=>"kcc888","CharacterSet" => "UTF-8", 'ReturnDatesAsStrings'=> true);
        $conn = sqlsrv_connect( $serverName, $connectionInfo);
        return $conn;
    }

    //判斷是否已有該公司
    public function if_in_database($array, $data, $conn) {
        for ($i=0; $i < count($data); $i++) { 
            $sql = "SELECT * from Company where 公司簡稱='".$array[$i]['cmpname']."';";
            $company_list = sqlsrv_query($conn, $sql); 
            if($company_list === false) {
                //將該公司新增至資料庫
                die( print_r( sqlsrv_errors(), true) );
            }
            else{
                $temp = sqlsrv_fetch_array($company_list);
                if ($temp[0]!=null) {
                //原資料庫有資料, $temp[0]是CompanyID, $temp[5]是公司簡稱
                    //把CompanyID放進陣列
                    array_push($data[$i], $temp[0]);
                } else {
                    array_push($data[$i], null);
                }
            }
        }
        return $data;
    }

    //更新股價、公司資訊等等
    public function update_info($array, $data, $conn) {

        //找該公司ID最新資料的日期 為當日則update,否則insert
        if ($data[15]!=null) {
            
            $sql = "SELECT Top 1 * FROM Shares WHERE CompanyID = ".$data[16]." ORDER BY [Date] DESC";
            echo "<br>".$sql;
            
            $company_list = sqlsrv_query($conn, $sql);
            $temp = sqlsrv_fetch_array($company_list);

            $data_date = substr($temp[5], 0, 10);
            $today = date("Y-m-d");
            if ($data_date == $today) {
            //最新日期為當日 用update
                $sql = "UPDATE Shares SET AP = ".$data[4].", SP = ".$data[11].", [Date] = CONVERT(varchar(100), GETDATE(), 23) WHERE SharesID = ".$temp[0];
                echo "<br>".$sql;
                sqlsrv_query($conn, $sql); 
            } else {
            //最新日期非當日 用insert
                $sql = "INSERT into Shares (CompanyID, AP, SP, PS, [Date]) values (".$data[16].", ".$data[4].", ".$data[11].", NULL, CONVERT(varchar(100), GETDATE(), 23) )";
                echo "<br>".$sql;
                sqlsrv_query($conn, $sql); 
            }
            echo "<br>";
        }
    }


    //新增一間公司資料至資料庫
    public function insert_info($data, $conn) {
        // print_r($data);
        $type = '新增一間公司資料至資料庫';
        $target = substr_replace($data[15] ,'Cmpinfo' ,36 ,13);
        $table = $this->parse($type, $target);

        //echo $table->plaintext;
        foreach($table->find('tr') as $row) {
            // initialize array to store the cell data from each row
            $flight = array();
            foreach($row->find('td') as $cell) {
                // push the cell's text to the array
                $flight[] = $cell->plaintext;
            }
            $rowData[] = $flight;
        }

        $cmp_info = array();
        // echo "<br>";
        for ($i=0; $i < count($rowData); $i++) { 
            // echo "<br>-------陣列".$i."<br>";
            for ($j=0; $j < count($rowData[$i]); $j++) { 
                if ($i!=58 && $i!=45 && $j%2 == 1) {
                    array_push($cmp_info, $rowData[$i][$j]);
                }
                // echo "<br>--項目".$j."<br>";
                // print_r($rowData[$i][$j]);
                // echo "<br>";
            }
            // echo "<br>";
        }

        for ($i=0; $i < count($cmp_info); $i++) { 
            echo "<br>第".$i."項".$cmp_info[$i];
        }


        $sql = "INSERT INTO [dbo].[Company]
           ([股票代號]
           ,[公司名稱]
           ,[公司簡稱]
           ,[公司成立日期]
           ,[公開發行日期]
           ,[實收資本額]
           ,[營利事業統一編號]
           ,[總機]
           ,[地址]
           ,[網址]
           ,[主要經營業務]
           ,[董事長]
           ,[總經理]
           ,[發言人]
           ,[發言人電話]
           ,[代理發言人]
           ,[股票過戶機構]
           ,[電話]
           ,[過戶地址]
           ,[簽證會計師事務所]
           ,[上市日期]
           ,[輔導卷商]
           ,[備註]
           ,[資本額])
     OUTPUT inserted.CompanyID
     VALUES
           (".$cmp_info[1].",
           '".$cmp_info[2]."',
           '".$data[14]."',
           '".$cmp_info[3]."',
           '".$cmp_info[5]."',
           '".$cmp_info[18]."',
           '".$cmp_info[1]."',
           '".$cmp_info[15]."',
           '".$cmp_info[12]."',
           '".$cmp_info[14]."',
           '".$cmp_info[24]."',
           '".$cmp_info[4]."',
           '".$cmp_info[6]."',
           '".$cmp_info[8]."',
           '".$cmp_info[13]."',
           '".$cmp_info[10]."',
           '".$cmp_info[25]."',
           '".$cmp_info[26]."',
           '".$cmp_info[27]."',
           '".$cmp_info[29]."',
           '".$cmp_info[32]."',
           '".$cmp_info[35]."',
           '".$cmp_info[58]."',
           '".$cmp_info[16]."')";

        echo "<br>".$sql;
        sqlsrv_query($conn, $sql); 
    }

    public function Parse_setting() {
        $this->load->Model("Parse_model");//載入model
        $setting_list = $this->Parse_model->get_parse_setting();//抓setting
        // print_r($setting_list);
        $this->load->view('parse_setting',array('setting_list' => $setting_list));
    }

    public function set() {
        $this->load->Model("Parse_model");//載入model
        $change = '+'.$_POST['stophours'].' hours';
        $new_time = date("Y-m-d H:i:s", strtotime($change));
        echo $new_time;
        $this->Parse_model->set_parse_setting($_POST['cpyname'], $new_time);

        $this->Parse_setting();
    }

    public function delete_setting() {
        $this->load->Model("Parse_model");//載入model
        $this->Parse_model->delete_setting($_POST['id']);
        $this->Parse_setting();
    }

}
