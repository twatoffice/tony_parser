USE [kcc888]
GO
/****** Object:  Table [dbo].[Company]    Script Date: 2018/5/4 下午 03:59:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Company](
	[CompanyID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[股票代號] [nvarchar](50) NULL,
	[第一個字注音] [nvarchar](1) NULL,
	[第二個字注音] [nvarchar](1) NULL,
	[公司名稱] [nvarchar](150) NULL,
	[公司簡稱] [varchar](50) NULL,
	[普通股] [nvarchar](150) NULL,
	[特別股] [nvarchar](150) NULL,
	[公司成立日期] [nvarchar](50) NULL,
	[公開發行日期] [nvarchar](50) NULL,
	[實收資本額] [nvarchar](50) NULL,
	[上市日期] [nvarchar](50) NULL,
	[上櫃日期] [nvarchar](50) NULL,
	[興櫃日期] [nvarchar](50) NULL,
	[營利事業統一編號] [nvarchar](50) NULL,
	[總機] [nvarchar](50) NULL,
	[傳真機號] [nvarchar](50) NULL,
	[地址] [nvarchar](255) NULL,
	[網址] [nvarchar](255) NULL,
	[電子郵件信箱] [nvarchar](255) NULL,
	[主要經營業務] [nvarchar](max) NULL,
	[董事長] [nvarchar](50) NULL,
	[總經理] [nvarchar](50) NULL,
	[發言人] [nvarchar](50) NULL,
	[發言人職稱] [nvarchar](50) NULL,
	[發言人電話] [nvarchar](50) NULL,
	[代理發言人] [nvarchar](50) NULL,
	[最後核准變更日期] [nvarchar](50) NULL,
	[股票過戶機構] [nvarchar](50) NULL,
	[電話] [nvarchar](70) NULL,
	[過戶地址] [nvarchar](max) NULL,
	[簽證會計師事務所] [nvarchar](50) NULL,
	[輔導卷商] [nvarchar](50) NULL,
	[特別股發行] [nvarchar](3) NULL,
	[公司債發行] [nvarchar](3) NULL,
	[備註] [nvarchar](max) NULL,
	[Type] [nvarchar](1) NULL,
	[SSMA_TimeStamp] [timestamp] NOT NULL,
	[MoneyBook] [nvarchar](max) NULL,
	[CompanyDir_ID] [int] NULL,
	[輔導日期] [nvarchar](50) NULL,
	[kcc888Hot] [bit] NULL,
	[kcc888Sn] [int] NULL,
	[kcc888成交排行] [int] NULL,
	[kcc888Visible] [bit] NULL,
	[MoneynewsHot] [bit] NULL,
	[MoneynewsSn] [int] NULL,
	[Moneynews成交排行] [int] NULL,
	[MoneynewsVisible] [bit] NULL,
	[WinnerHot] [bit] NULL,
	[WinnerSn] [int] NULL,
	[Winner成交排行] [int] NULL,
	[WinnerVisible] [bit] NULL,
	[BejaHot] [bit] NULL,
	[BejaSn] [int] NULL,
	[Beja成交排行] [int] NULL,
	[BejaVisible] [bit] NULL,
	[大股東持股比率] [nvarchar](max) NULL,
	[上市櫃價格] [nvarchar](50) NULL,
	[資本額] [nvarchar](50) NULL,
	[kcc888個股首頁備註] [nvarchar](200) NULL,
	[Beja個股首頁備註] [nvarchar](200) NULL,
	[CompanyFather_ID] [int] NULL,
	[WinnerBackGround] [nvarchar](200) NULL,
	[WinnerHomeHot] [int] NULL,
	[Winner個股首頁備註] [nvarchar](200) NULL,
	[WinnerPSBuySheet] [nvarchar](5) NULL,
	[WinnerPSSellSheet] [nvarchar](5) NULL,
	[WinnerPSBuyCount] [nvarchar](5) NULL,
	[WinnerPSSellCount] [nvarchar](5) NULL,
	[WinnerShareownerType] [nchar](1) NULL,
	[WinnerShareownerContent] [nvarchar](200) NULL,
	[WinnerShareownerType2] [nchar](1) NULL,
	[WinnerShareownerContent2] [nvarchar](200) NULL,
	[WinnerShareownerType3] [nchar](1) NULL,
	[WinnerShareownerContent3] [nvarchar](200) NULL,
	[四字簡稱] [nvarchar](7) NULL,
	[股東會日期開始] [datetime] NULL,
	[股東會日期結束] [datetime] NULL,
	[Moneynews個股首頁備註] [nvarchar](500) NULL,
	[SharesAPType] [nchar](3) NULL,
	[SharesSPType] [nchar](3) NULL,
	[統一價議] [bit] NULL,
	[Bio] [bit] NULL,
	[Good588Sn] [int] NULL,
	[Good588Visible] [bit] NULL,
	[Good588Hot] [bit] NULL,
	[Good588成交排行] [nchar](10) NULL,
	[Good588個股首頁備註] [nvarchar](500) NULL,
	[Fa888Sn] [int] NULL,
	[Fa888Visible] [bit] NULL,
	[Fa888Hot] [bit] NULL,
	[Tw88588Sn] [int] NULL,
	[Tw88588Visible] [bit] NULL,
	[Tw88588Hot] [bit] NULL,
	[Tw88588成交排行] [nchar](3) NULL,
	[Tw88588個股首頁備註] [nvarchar](50) NULL,
	[estockHomeHot] [int] NULL,
	[EstockSn] [int] NULL,
 CONSTRAINT [Company$PrimaryKey] PRIMARY KEY CLUSTERED 
(
	[CompanyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[Company] ADD  CONSTRAINT [DF__Company__Type__1CF15040]  DEFAULT ((1)) FOR [Type]
GO
ALTER TABLE [dbo].[Company] ADD  CONSTRAINT [DF_Company_kcc888Hot]  DEFAULT ((0)) FOR [kcc888Hot]
GO
ALTER TABLE [dbo].[Company] ADD  CONSTRAINT [DF__Company__Sn__1B0907CE]  DEFAULT ((0)) FOR [kcc888Sn]
GO
ALTER TABLE [dbo].[Company] ADD  CONSTRAINT [DF__Company__成交排行__1BFD2C07]  DEFAULT ((0)) FOR [kcc888成交排行]
GO
ALTER TABLE [dbo].[Company] ADD  CONSTRAINT [DF__Company__Visible__1DE57479]  DEFAULT ((1)) FOR [kcc888Visible]
GO
ALTER TABLE [dbo].[Company] ADD  CONSTRAINT [DF_Company_MoneynewsHot]  DEFAULT ((0)) FOR [MoneynewsHot]
GO
ALTER TABLE [dbo].[Company] ADD  CONSTRAINT [DF_Company_MoneynewsSn]  DEFAULT ((0)) FOR [MoneynewsSn]
GO
ALTER TABLE [dbo].[Company] ADD  CONSTRAINT [DF_Company_Moneynews成交排行]  DEFAULT ((0)) FOR [Moneynews成交排行]
GO
ALTER TABLE [dbo].[Company] ADD  CONSTRAINT [DF_Company_MoneynewsVisible]  DEFAULT ((1)) FOR [MoneynewsVisible]
GO
ALTER TABLE [dbo].[Company] ADD  CONSTRAINT [DF_Company_WinnerHot]  DEFAULT ((0)) FOR [WinnerHot]
GO
ALTER TABLE [dbo].[Company] ADD  CONSTRAINT [DF_Company_WinnerSn]  DEFAULT ((0)) FOR [WinnerSn]
GO
ALTER TABLE [dbo].[Company] ADD  CONSTRAINT [DF_Company_Winner成交排行]  DEFAULT ((0)) FOR [Winner成交排行]
GO
ALTER TABLE [dbo].[Company] ADD  CONSTRAINT [DF_Company_WinnerVisible]  DEFAULT ((1)) FOR [WinnerVisible]
GO
ALTER TABLE [dbo].[Company] ADD  CONSTRAINT [DF_Company_BejaHot]  DEFAULT ((0)) FOR [BejaHot]
GO
ALTER TABLE [dbo].[Company] ADD  CONSTRAINT [DF_Company_BejaSn]  DEFAULT ((0)) FOR [BejaSn]
GO
ALTER TABLE [dbo].[Company] ADD  CONSTRAINT [DF_Company_Beja成交排行]  DEFAULT ((0)) FOR [Beja成交排行]
GO
ALTER TABLE [dbo].[Company] ADD  CONSTRAINT [DF_Company_BejaVisible]  DEFAULT ((1)) FOR [BejaVisible]
GO
ALTER TABLE [dbo].[Company] ADD  CONSTRAINT [DF_Company_CompanyFather_ID]  DEFAULT ((0)) FOR [CompanyFather_ID]
GO
ALTER TABLE [dbo].[Company] ADD  CONSTRAINT [DF_Company_統一價議]  DEFAULT ((0)) FOR [統一價議]
GO
ALTER TABLE [dbo].[Company] ADD  CONSTRAINT [DF_Company_Bio]  DEFAULT ((0)) FOR [Bio]
GO
ALTER TABLE [dbo].[Company] ADD  CONSTRAINT [DF_Company_Good588Sn]  DEFAULT ((0)) FOR [Good588Sn]
GO
ALTER TABLE [dbo].[Company] ADD  CONSTRAINT [DF_Company_GoodVisible]  DEFAULT ((0)) FOR [Good588Visible]
GO
ALTER TABLE [dbo].[Company] ADD  CONSTRAINT [DF_Company_Fa888Sn]  DEFAULT ((0)) FOR [Fa888Sn]
GO
ALTER TABLE [dbo].[Company] ADD  CONSTRAINT [DF_Company_Fa888Visible]  DEFAULT ((0)) FOR [Fa888Visible]
GO
ALTER TABLE [dbo].[Company] ADD  CONSTRAINT [DF_Company_Fa888Hot]  DEFAULT ((0)) FOR [Fa888Hot]
GO
ALTER TABLE [dbo].[Company] ADD  CONSTRAINT [DF_Company_Tw88588Sn]  DEFAULT ((0)) FOR [Tw88588Sn]
GO
ALTER TABLE [dbo].[Company] ADD  CONSTRAINT [DF_Company_Tw88588Visible]  DEFAULT ((0)) FOR [Tw88588Visible]
GO
ALTER TABLE [dbo].[Company] ADD  CONSTRAINT [DF_Company_Tw88588Hot]  DEFAULT ((0)) FOR [Tw88588Hot]
GO
ALTER TABLE [dbo].[Company] ADD  CONSTRAINT [DF_Company_EstockSn]  DEFAULT ((0)) FOR [EstockSn]
GO
